const commandInput = document.querySelector('#short-input')
const firstInsertInput = document.querySelector('#full-input')
const history = document.querySelector('#history')
const backend = new WebSocket('wss://ed-api.apps.penalosa.dev')
commandInput.value = ''
commandInput.focus()
backend.onclose = () => {
  document.body.innerHTML = `
		<div class="error">
			<h1>
				?
			</h1>
		</div>
		${document.body.innerHTML}`
}
backend.onmessage = (m) => {
  m.data
    .split('\n')
    .slice(0, -1)
    .forEach((l) => {
      let div = document.createElement('div')
      div.textContent = l
      div.className = 'response-data'
      history.append(div)
    })
  commandInput.scrollIntoView({ behavior: 'smooth' })
}
commandInput.addEventListener('keypress', (e) => {
  if (e.which == 13) {
    backend.send(commandInput.value + '\n')
    let div = document.createElement('div')
    div.textContent = commandInput.value
    div.className = 'command-data'
    history.append(div)
    if (
      commandInput.value.endsWith('i') ||
      commandInput.value.endsWith('a') ||
      commandInput.value.endsWith('c')
    ) {
      commandInput.blur()
      firstInsertInput.disabled = false
      firstInsertInput.style.display = 'block'
      firstInsertInput.value = ''
      firstInsertInput.focus()
    }
    commandInput.value = ''
  }
})
document.addEventListener('keypress', (e) => {
  if (e.which == 13 && e.target.className == 'insert') {
    if (e.target.value == '.') {
      let value =
        [...document.querySelectorAll('.insert')]
          .map((e) => e.value)
          .join('\n') + '\n'
      ;[...document.querySelectorAll('.insert')]
        .filter((e) => e.id != 'full-input')
        .map((e) => e.remove())
      commandInput.focus()
      firstInsertInput.value = ''
      firstInsertInput.style.display = 'none'
      firstInsertInput.disabled = true

      backend.send(value)
      console.log(value)
    } else {
      let next = document.createElement('input')
      next.className = 'insert'
      e.target.disabled = true
      e.target.insertAdjacentElement('afterend', next)
      next.focus()
    }
  }
})
