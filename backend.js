const { spawn } = require("child_process");
const WebSocket = require("ws");

const run = () => {
	const wss = new WebSocket.Server({ port: 8081 });

	wss.on("connection", ws => {
		setupConnection(ws);
	});
};
run();
const setupConnection = ws => {
	let ed = spawn("ed");
	let open = true;
	ed.stdout.on("data", data => {
		ws.send(`${data}`);
	});

	ed.on("close", code => {
		open = false;
		ws.close();
	});
	ws.on("message", message => {
		if (open) {
			ed.stdin.write(message);
		}
	});

	ws.send("Welcome to Ed on the Web!\n");
};
